var imageApple = new Image(25, 25);
imageApple.src = './Apfel.png';

var imageback = new Image(500, 500);
imageback.src = './background.png';



var imageKopf = new Image(25,25);
imageKopf.src = './sKopfUP.png';

var imageKopfU = new Image(25, 25);
imageKopfU.src = './sKopfUP.png';

var imageKopfD = new Image(25, 25);
imageKopfD.src = './sKopfDOWN.png';

var imageKopfR = new Image(25, 25);
imageKopfR.src = './sKopfRIGHT.png';

var imageKopfL = new Image(25, 25);
imageKopfL.src = './sKopfLEFT.png';



var imageMitteLR = new Image(25, 25);
imageMitteLR.src = './middleLR.png';

var imageMitteUD = new Image(25, 25);
imageMitteUD.src = './middle.png';




var imageCurveUL = new Image(25, 25);
imageCurveUL.src = './curveUL.png';

var imageCurveUR = new Image(25, 25);
imageCurveUR.src = './curveUR.png';

var imageCurveDL = new Image(25, 25);
imageCurveDL.src = './curveDL.png';

var imageCurveDR = new Image(25, 25);
imageCurveDR.src = './curveDR.png';





var imageTailU = new Image(25, 25);
imageTailU.src = './tailUP.png';

var imageTailR = new Image(25, 25);
imageTailR.src = './tailRI.png';

var imageTailD = new Image(25, 25);
imageTailD.src = './tailDO.png';

var imageTailL = new Image(25, 25);
imageTailL.src = './tailLE.png';


//board
var blocksize = 25;
var rows = 20; // also 20 reihen
var cols = 20; // also 20 spalten
var board;
var boardScore;
var gameOverElement;
var context; // damit malt man dann 
var velocityX = 0;
var velocityY = 0;
let score = 0;
var gameId;

// food
var foodX;
var foodY;

// snake head
var snakeX = blocksize * 1; // start vom Kopf bei x=5
var snakeY = blocksize * 1; // start vom Kopf bei y=5

//API
const apiUrl = 'http://localhost:4000/scoreboard-api';
var highscoresElement;
var highscores = [];

var gameOver = false;
var snakeBody = [];




class Player {
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }
    logProperties() {
        console.log(this.name);
        console.log(this.score);
    }
}



window.onload = function () {
    boardScore = document.getElementById("score");
    gameOverElement = document.getElementById("game-over-container");

    board = document.getElementById("board");
    highscoresElement = document.getElementById("scoreBoard");

    board.height = rows * blocksize;
    board.width = cols * blocksize;
    context = board.getContext("2d");

    document.addEventListener('swiped', function (e) {
        changeDirection(e.detail.dir);
    });

    document.addEventListener("keydown", changeDirectionKey);
    resetGame()

}

function changeDirectionKey(e) {
    if ((e.code == "ArrowUp" || e.code == "KeyW")) {
        changeDirection("up");
    }

    else if ((e.code == "ArrowDown" || e.code == "KeyS")) {
        changeDirection("down");
    }

    else if ((e.code == "ArrowLeft" || e.code == "KeyA")) {
        changeDirection("left");
    }

    else if ((e.code == "ArrowRight" || e.code == "KeyD")) {
        changeDirection("right");
    }
}



function update() {
    if(gameOver){
        return
    }
    context.fillStyle = "black";  // spielfeld wird schwarz gefärbt
    context.drawImage(imageback,0, 0, board.width, board.height);


    context.fillStyle = "red"; // food (apfel) hat Farbe rot
    context.drawImage(imageApple,foodX, foodY, blocksize, blocksize);


    if (snakeX == foodX && snakeY == foodY) {
        snakeBody.push([foodX, foodY]);
        placeFood();
        score += 1
    }

    for (let i = snakeBody.length - 1; i > 0; i--) {
        snakeBody[i] = snakeBody[i - 1]; // letztes körpersegment geht auf den Platz von dem davor 
    }
    if (snakeBody.length > 0) {
        snakeBody[0] = [snakeX, snakeY, velocityX, velocityY];
    }

    context.fillStyle = "blue";
    snakeX += velocityX * blocksize;
    snakeY += velocityY * blocksize;


    context.drawImage(imageKopf, snakeX, snakeY, blocksize, blocksize);
    
    
    for (let i = 0; i < snakeBody.length; i++) {
        if(i === snakeBody.length -1){
            if(snakeBody[i][2] === 0 && snakeBody[i][3] === -1 ){
                context.drawImage(imageTailU, snakeBody[snakeBody.length-1][0], snakeBody[snakeBody.length-1][1], blocksize, blocksize);
            }
            if(snakeBody[i][2] === 0 && snakeBody[i][3] === 1){
                context.drawImage(imageTailD, snakeBody[snakeBody.length-1][0], snakeBody[snakeBody.length-1][1], blocksize, blocksize);
            }
            if(snakeBody[i][2] === -1 && snakeBody[i][3] === 0){
                context.drawImage(imageTailL, snakeBody[snakeBody.length-1][0], snakeBody[snakeBody.length-1][1], blocksize, blocksize);
            }
            if(snakeBody[i][2] === 1 && snakeBody[i][3] === 0){
                context.drawImage(imageTailR, snakeBody[snakeBody.length-1][0], snakeBody[snakeBody.length-1][1], blocksize, blocksize);
            }
        }else{
            if(snakeBody[i][2] === 0 && snakeBody[i][3] === -1 ){
                context.drawImage(imageMitteUD, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
            }
            if(snakeBody[i][2] === 0 && snakeBody[i][3] === 1){
                context.drawImage(imageMitteUD, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
            }
            if(snakeBody[i][2] === -1 && snakeBody[i][3] === 0){
                
                context.drawImage(imageMitteLR, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
            }
            if(snakeBody[i][2] === 1 && snakeBody[i][3] === 0){
                context.drawImage(imageMitteLR, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
            }
        }
        
    }

    /**
     * snakebody [x, y, velocityX, velocityY]
     */

    // for (let i = 0; i < snakeBody.length; i++) {
    //     // if tail  
    //     if (i === snakeBody.length - 1) {
    //         if (snakeBody[i][2] === 0 && snakeBody[i][3] === -1) {
    //             context.drawImage(imageTailU, snakeBody[snakeBody.length - 1][0], snakeBody[snakeBody.length - 1][1], blocksize, blocksize);
    //         }
    //         if (snakeBody[i][2] === 0 && snakeBody[i][3] === 1) {
    //             context.drawImage(imageTailD, snakeBody[snakeBody.length - 1][0], snakeBody[snakeBody.length - 1][1], blocksize, blocksize);
    //         }
    //         if (snakeBody[i][2] === -1 && snakeBody[i][3] === 0) {
    //             context.drawImage(imageTailL, snakeBody[snakeBody.length - 1][0], snakeBody[snakeBody.length - 1][1], blocksize, blocksize);
    //         }
    //         if (snakeBody[i][2] === 1 && snakeBody[i][3] === 0) {
    //             context.drawImage(imageTailR, snakeBody[snakeBody.length - 1][0], snakeBody[snakeBody.length - 1][1], blocksize, blocksize);
    //         }
    //     } else {
    //         // zweites glied, kopf ist vor uns
    //         if (i === 0) {

    //         } else {
    //             var vorherigesGlied = snakeBody[i - 1];
    //             var nachfolgendesGlied = snakeBody[i + 1];
    //             if (vorherigesGlied[2] !== nachfolgendesGlied[2] || vorherigesGlied[3] !== nachfolgendesGlied[3]) {
    //                 // kurve
                    
    //             } else {
    //                 if (snakeBody[i][2] === 0 && snakeBody[i][3] === -1) {
    //                     context.drawImage(imageMitteUD, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
    //                 }
    //                 if (snakeBody[i][2] === 0 && snakeBody[i][3] === 1) {
    //                     context.drawImage(imageMitteUD, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
    //                 }
    //                 if (snakeBody[i][2] === -1 && snakeBody[i][3] === 0) {

    //                     context.drawImage(imageMitteLR, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
    //                 }
    //                 if (snakeBody[i][2] === 1 && snakeBody[i][3] === 0) {
    //                     context.drawImage(imageMitteLR, snakeBody[i][0], snakeBody[i][1], blocksize, blocksize);
    //                 }
    //             }
    //         }
    //     }

    // }


    // game over conditions
    if (snakeX < 0 || snakeX > cols * blocksize || snakeY < 0 || snakeY > rows * blocksize) {
        onGameOver();
        return;
    }
    for (let i = 0; i < snakeBody.length; i++) {
        if (snakeX == snakeBody[i][0] && snakeY == snakeBody[i][1]) {
            onGameOver();
            return;
        }
    }

    // score
    context.fillStyle = "blue"
    context.font = "45px sans-serif"
    context.fillText(score, 5, 45);

}

async function onGameOver() {
    gameOver = true;
    gameOverElement.className = '';
    boardScore.innerText = score;
    clearInterval(gameId);
    var resultJsonString;

    await fetch(apiUrl)
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error("Could not load data");
            }
        })
        .then(data => {
            resultJsonString = JSON.stringify(data);
        })
        .catch((error) => {
            resultJsonString = "";
        });
    parseScoreboard(resultJsonString);
    drawScoreboard();
}

function parseScoreboard(data) {
    highscores = [];
   
    if(data == ""){
        return;
    }
    var scores = JSON.parse(data).scores;

    scores.forEach((player) => {
        const newPlayer = new Player(player.user, player.score);
        highscores.push(newPlayer);
    })
    highscores = highscores.sort((a, b) => {
        const scoreA = a.score;
        const scoreB = b.score;
        if (scoreA > scoreB) {
            return -1;
        }
        if (scoreA < scoreB) {
            return 1;
        }
        return 0;
    })
}

function drawScoreboard() {
    highscores.forEach((player) => {
        highscoresElement.innerHTML = highscoresElement.innerHTML + player.name + ":" + player.score + "</br>"
    })
}

function onRestartClick() {
    resetGame();
}

async function onSubmitClick() {
    const nameValue = document.getElementById("name").value;
    const sc = score;
    console.log(nameValue);
    console.log(sc)    
}



function resetGame() {
    snakeX = blocksize * 1; // start vom Kopf bei x=5
    snakeY = blocksize * 1; // start vom Kopf bei y=5
    velocityX = 0;
    velocityY = 0;
    gameOver = false;
    score = 0;
    snakeBody = [];

    gameOverElement.className = 'hidden';
    placeFood();
    gameId = setInterval(update, 100);// alle 100 millisek. update vom canvas (der Leinwand)

}


function changeDirection(dir) {
    if (dir == "up"  && velocityY != 1) {
        imageKopf = imageKopfU;
        imageMitte = imageMitteUD;
        imageTail = imageTailU;
        velocityX = 0;
        velocityY = -1;
    }

    else if (dir == "down"  && velocityY != -1) {
        imageKopf = imageKopfD;
        imageMitte = imageMitteUD;
        imageTail = imageTailD;
        velocityX = 0;
        velocityY = 1;
    }

    else if (dir == "left" && velocityX != 1) {
        imageKopf = imageKopfL;
        imageMitte = imageMitteLR;
        imageTail = imageTailL;
        velocityX = -1;
        velocityY = 0;
    }

    else if (dir == "right" && velocityX != -1) {
        imageKopf = imageKopfR;
        imageMitte = imageMitteLR;
        imageTail = imageTailR;
        velocityX = 1;
        velocityY = 0;
    }
}

function placeFood() { // Zufallszahl durch Mathematik
    let placed = false;
    while (placed == false) {
        foodX = Math.floor(Math.random() * (cols - 1)) * blocksize;
        foodY = Math.floor(Math.random() * (rows - 1)) * blocksize;
        placed = true;
        snakeBody.forEach((bodyPart) => {
            if (foodX == bodyPart[0] && foodY == bodyPart[1]) {
                placed = false;
            }
        })
    }
}

